export const elem = (
  type,
  classNames,
  content,
  parent,
  attr = {},
  prepend = false
) => {
  const el = document.createElement(type);
  classNames &&
    classNames.split(" ").forEach((className) => el.classList.add(className));
  content && el.appendChild(document.createTextNode(content));
  attr &&
    Object.keys(attr).forEach((attrKey) => {
      el.setAttribute(attrKey, attr[attrKey]);
    });
  if (parent) {
    if (prepend) {
      parent.prepend(el);
    } else {
      parent.append(el);
    }
  }
  return el;
};
