import Router from "./class/Router.js";
import initTerminal from "./pages/terminal/terminal.js";
import { elem } from "./common/helpers.js";

window.addEventListener("load", function () {
  let loadingScreenMain = document.querySelector(".loading-main");

  setTimeout(() => {
    const loadingScreenSvg = document.querySelector(".loading-main svg");
    loadingScreenSvg.classList.add("active");
    setTimeout(() => {
      window.router = new Router({
        baseUrl,
        beforeLoad,
        afterLoad,
        pageInitFns,
      });
      loadingScreenMain.style.display = "none";
    }, 1400); //duration of total animation is 1.8s currently
  }, 1000); //mocked loading time 1s
});

const baseUrl = window.location.origin;
const pageInitFns = [
  {
    path: ["/", "/index.html"],
    fn: () => console.log("Init fn of index.html"),
  },
  {
    path: ["/terminal.html", "/terminal"],
    fn: initTerminal,
  },
];
const beforeLoad = () => {
  createLoader();
};
const afterLoad = () => {
  deleteLoader();
};

const createLoader = () => {
  const topLoader = elem("DIV", "top-loader");
  elem("DIV", "loader", "", topLoader);
  elem("DIV", "loader", "", topLoader);
  topLoader.animate(
    {
      opacity: [0, 1],
    },
    50
  );
};

const deleteLoader = () => {
  const topLoader = document.querySelector(".top-loader");
  if (topLoader) {
    const topLoaderFadeOut = topLoader.animate(
      {
        opacity: [1, 0],
      },
      50
    );

    topLoaderFadeOut.addEventListener("finish", () => {
      topLoader.parentElement.removeChild(topLoader);
    });
  }
};
