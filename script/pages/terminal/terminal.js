import { elem } from "../../common/helpers.js";
import Webash from "./commands/webash.js";

const webashInstance = new Webash();
let PS1 = `user@${window.location.hostname}:~$ `;

export default function initTerminal() {
  createCommandOutput();
  eventListeners();
}

function createCommandOutput() {
  const terminalScreen = document.querySelector(".terminal-screen");
  const commandOutputElem = elem(
    "section",
    "command-output",
    "",
    terminalScreen
  );

  const inputPartElem = elem("form", "input-part", "", commandOutputElem);
  const outputPartElem = elem("div", "output-part", "", commandOutputElem);

  elem("div", "PS1", PS1, inputPartElem);
  const commandElem = elem("input", "command", "", inputPartElem, {
    type: "text",
    name: "command",
    autoComplete: "off",
  });

  inputPartElem.addEventListener("submit", (evt) => {
    evt.preventDefault();

    commandSubmit(commandElem, outputPartElem);
  });

  getFocusOnInput();
}

function eventListeners() {
  window.addEventListener("focus", getFocusOnInput);
  window.addEventListener("keydown", getFocusOnInput);
}

async function commandParser(command, outputPartElem) {
  if (command) {
    await webashInstance.executeExternalCommand(command, outputPartElem);
  }
}

async function commandSubmit(commandElem, outputPartElem) {
  const command = commandElem.value;

  // change previous command from input to div
  const latestInput = getCommandInput();
  if (latestInput) {
    latestInput.blur();
    elem("div", "command", commandElem.value, commandElem.parentElement);
    commandElem.parentElement.removeChild(commandElem);
  }

  // actually parse the command
  await commandParser(command, outputPartElem);

  // draw the input for next command
  createCommandOutput();
}

function getCommandInput() {
  return document.querySelector("input[name=command]");
}

function getFocusOnInput(evt) {
  const latestInput = getCommandInput();
  latestInput && latestInput.focus();
  if (evt) {
    if (evt.constructor.name === "KeyboardEvent") {
      if (evt.key === "ArrowUp") {
        evt.preventDefault();
        const history = webashInstance.getHistory() || "";
        latestInput && (latestInput.value = history);
      }
      if (evt.key === "ArrowDown") {
        evt.preventDefault();
        const history = webashInstance.getHistory(false) || "";
        latestInput && (latestInput.value = history);
      }
    } else if (evt.constructor.name === "FocusEvent") {
    }
  }
}
