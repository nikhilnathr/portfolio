import Command from "../Command.js";

export default class Clear extends Command {
  static name = "clear";
  static version = "0.0.1";
  static shortDescription = "Clear the terminal screen";

  async execute(args, output) {
    // clear the whole terminal screen
    const terminalScreen = document.querySelector(".terminal-screen");
    while (terminalScreen.firstChild) {
      terminalScreen.lastChild.remove();
    }
  }

  changeLogs() {
    const logs = {
      "0.0.1": "Clearing the screen completely",
    };
    return logs;
  }
}
