import Command from "../Command.js";

export default class Works extends Command {
  static name = "works";
  static version = "0.0.2";
  static shortDescription = "Display my works";

  async execute(args, output) {
    output.elem("a", "", "nikhilnathr.com", {
      href: "https://nikhilnathr.com",
      style: "display: block;",
    });
    output.elem("a", "", "Coronasafe Stay", {
      href: "https://stay.coronasafe.network",
      style: "display: block;",
    });
    output.elem("a", "", "Qubiso", {
      href: "https://qubiso.com",
      style: "display: block;",
    });
    output.elem("a", "", "Takomodo", {
      href: "https://dash.takomodo.com",
      style: "display: block;",
    });
  }

  changeLogs() {
    const logs = {
      "0.0.1": "Show the basic links to all my works.",
      "0.0.2":
        "Bugfix\nThe links had previously been relative links which are now replaced by absolute links.",
    };
    return logs;
  }
}
