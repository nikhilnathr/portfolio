import Command from "../Command.js";
import { elem } from "../../../common/helpers.js";

export default class Contribute extends Command {
  static name = "contribute";
  static version = "0.0.1";
  static shortDescription = "Short contribution guide for web terminal";

  async execute(args, output) {
    output.elem("h2", "", "Contribute to Web Terminal", {
      style: "margin-bottom: 20px;",
    });
    output.print(
      `You can write your own commands for this terminal and publish it in your page. You can also clone the repository and make a merge request to my repository to see it in my website. I always love people who contribute to my code or any code in general.\n`
    );

    // display important files
    output.elem("h3", "", "Files", { style: "margin-bottom: 10px;" });
    output.print(
      "The directories and files related to terminal in the project are "
    );
    const files = [
      {
        name: "terminal.html",
        description:
          "This is the actual HTML file of the project. All the static contents (elements not influenced / created by JS) are situated here.",
      },
      {
        name: "style/style.css",
        description:
          "The style related to the whole website is here. Better not change the values here and change the styles using JS js preffered. This is because the styles may conflict with other style in various commands.",
      },
      {
        name: "script/script.js",
        description:
          "Entry file JS for the whole webpage. Handles main routing, better not change this unless you need some routing modifications.",
      },
      {
        name: "script/pages/terminal/terminal.js",
        description:
          "Entry point for terminal.html. Controls the event listeners for the page, calls the webash to execute the commands and makes the prompt after each command is executed.",
      },
      {
        name: "script/pages/terminal/Command.js",
        description:
          "A basic class structure which can be extended when making commands. You can also make command class as a standalone one but it is safer to extend from this class as future additions to webash may intoduce a need for the command class to have to new methods which may not be present in your commands while writing now. Extending from this Command class ensures that your command will not break even if there is some changes with the webash file since the Command class will have a default fallback for such new methods",
      },
      {
        name: "script/pages/terminal/commands/",
        description:
          "This folder contains all the commands exected by webash. The commands files contain classes as default exports which extends the Command class (not necessary to extend but advisable).",
      },
    ];
    const ulFiles = elem("ul", "", "", null, {
      type: "disk",
      style: "padding-left: 20px; margin-top: 5px;",
    });
    files.forEach((file) => {
      ulFiles.appendChild(this.showMoreMenu(file.name, file.description));
    });
    output.append(ulFiles);
    output.print();

    // explain the basics of making a command
    output.elem("h3", "", "Brief Explaination", {
      style: "margin-bottom: 10px;",
    });
    output.print("New commands can be placed under the ", false);
    output.elem("i", "", "script/pages/terminal/commands/", {
      style: "font-weight: 800;",
    });
    output.print(
      " folder. The file name of the command is advisable to match the command name. The file should contain a command class containing a static variable ",
      false
    );
    output.elem("i", "", "shortDescription", { style: "font-weight: 800;" });
    output.print(
      ", which is a short description of the command which is shown in webash help. It shoult also conatain a asynchronous method ",
      false
    );
    output.elem("i", "", "execute(args, output)", {
      style: "font-weight: 800;",
    });
    output.print(
      ", this is the method called when the command is executed. This function has 2 parameters args which is the arguments passed to the function and the second is an output object which is used to ouput to the page. Extend the Command class which makes default assignments for default functions which may be required in the future.\n"
    );

    output.print(
      "After writing the class for the command, you should import it in the webash file and mention it in the constructor under executables. Executables is an object, the key is the word used to invoke the command and the value is the class of the command.\n"
    );

    output.print("The whole code for this webpage can be seen in ", false);
    output.elem("a", "", "gitlab repository", {
      href: "https://gitlab.com/nikhilnathr/portfolio",
    });
  }

  showMoreMenu(item, child) {
    const showMoreElem = elem("li", "", "", null, {
      style: "padding-bottom: 5px;",
    });

    const titleElem = elem("div", "", item, showMoreElem);
    const moreTextElem = elem("span", "", "[Show more...]", titleElem, {
      style: "cursor: pointer;text-decoration: underline; margin-left: 10px;",
    });

    const childElem = elem("div", "", child, showMoreElem, {
      style: "overflow: hidden; height: 0;",
    });

    const paddingVal = "5px";

    let expanded = false;

    moreTextElem.addEventListener("click", () => {
      if (expanded) {
        expanded = false;
        childElem
          .animate(
            {
              height: [`${childElem.offsetHeight}px`, 0],
              paddingTop: [paddingVal, 0],
              paddingBottom: [paddingVal, 0],
            },
            {
              duration: 300,
              easing: "ease-in-out",
            }
          )
          .addEventListener("finish", () => {
            childElem.style.height = 0;
            childElem.style.paddingTop = "";
            childElem.style.paddingBottom = "";
          });
        moreTextElem.textContent = "[Show more...]";
      } else {
        expanded = true;
        childElem.style.height = "";
        childElem.style.paddingTop = paddingVal;
        childElem.style.paddingBottom = paddingVal;
        childElem.animate(
          {
            height: [0, `${childElem.offsetHeight}px`],
            paddingTop: [0, paddingVal],
            paddingBottom: [0, paddingVal],
          },
          {
            duration: 300,
            easing: "ease-in-out",
          }
        );
        moreTextElem.textContent = "[Show less...]";
      }
    });

    return showMoreElem;
  }

  changeLogs() {
    const logs = {
      "0.0.1":
        "Add how to contribute message\nA show more menu with animation is implemented.",
    };
    return logs;
  }
}
