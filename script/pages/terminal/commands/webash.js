import { elem } from "../../../common/helpers.js";
import Command from "../Command.js";
import Works from "./works.js";
import Contribute from "./contribute.js";
import Clear from "./clear.js";

export default class Webash extends Command {
  static name = "webash";
  static version = "0.0.3";
  static shortDescription =
    "The executer for the terminal (like bash but very very simple)";

  constructor() {
    super();
    this.currentHistoryLevel = 0;
    this.history = JSON.parse(localStorage.getItem(".webash_history")) || [];
    this.executables = {
      webash: Webash,
      works: Works,
      contribute: Contribute,
      clear: Clear,
    };
  }

  async execute(args, output) {
    if (args.length > 0) {
      return this.executeExternalCommand(args.join(" "), output.el);
    }
  }

  help(output) {
    output.print(`${Webash.name} version ${Webash.version}\n`);
    output.print("Available commands");

    Object.keys(this.executables).forEach((command) => {
      output.print(
        `${command} - ${this.executables[command].shortDescription}`
      );
    });
  }

  changeLogs() {
    const logs = {
      "0.0.1":
        "Basic running of commands can be done.\nAdded the argument 'help' for listing all commands and their short description.",
      "0.0.2":
        "Added history module\nAdded name and version static variables\nIntroduced the change log.",
      "0.0.3":
        "Added history module\nAdded name and version static variables\nIntroduced the change log.",
    };
    return logs;
  }

  async executeExternalCommand(command, outputPartElem) {
    const splitCommand = this.splitCommand(command);
    this.currentHistoryLevel = 0;
    if (
      this.history[this.history.length - 1] !== command.trim() &&
      command[0] !== " "
    ) {
      this.history.push(command);
    }
    localStorage.setItem(".webash_history", JSON.stringify(this.history));
    const output = new Output(outputPartElem);

    const mainCommand = splitCommand[0];
    const args = splitCommand.slice(1);

    const executable = this.executables[mainCommand];

    let returnValue;
    if (executable) {
      returnValue = await new executable().execute(args, output);
    } else if (mainCommand === "help") {
      this.help(output);
      returnValue = 0;
    } else {
      output.print(`'${mainCommand}' command not found`);
      returnValue = 127;
    }

    return returnValue;
  }

  getHistory(backward = true) {
    if (backward) {
      if (this.currentHistoryLevel <= this.history.length) {
        this.currentHistoryLevel++;
      }
    } else {
      if (this.currentHistoryLevel > 0) {
        this.currentHistoryLevel--;
      }
    }
    return this.history[this.history.length - this.currentHistoryLevel];
  }

  splitCommand(command) {
    const args = [];
    // let prevSpace = true;
    let arg = "";
    for (let i = 0; i < command.length; i++) {
      const letter = command[i];
      if (letter === " ") {
        if (arg) {
          args.push(arg);
          arg = "";
        }
      } else {
        arg += letter;
      }
    }
    if (arg) {
      args.push(arg);
    }

    return args;
  }
}

class Output {
  constructor(el) {
    this.el = el;
  }

  print(str = "", newLine = true) {
    const contents = str.split("\n");

    contents.forEach((content) => {
      elem("span", "", content, this.el);
      if (newLine) {
        elem("br", "", "", this.el);
      }
    });
  }

  elem(type, className, content, attr) {
    return elem(type, className, content, this.el, attr);
  }

  append(el) {
    this.el.appendChild(el);
  }
}
