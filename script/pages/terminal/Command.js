export default class Command {
  static name = "";
  static version = "0.0.1";
  static shortDescription = "No description given";

  async execute(args, output) {
    output.print(
      `'${this.constructor.name}' called with args ${JSON.stringify(args)}`
    );
  }

  changeLogs() {
    const logs = {
      "0.0.1": "The command is introduced",
    };
    return logs;
  }
}
